package globals

import (
	"database/sql"
	jsoniter "github.com/json-iterator/go"
	"net/http"
	"time"
)

type AppStatus struct {
	Status    string `json:"status"`
	Connected bool   `json:"connected"`
}

type ApiKeys struct {
	User string
	Key  string
}

type ApplicationContext struct {
	JsonApi      jsoniter.API
	DbConnection *sql.DB

	DbUser     string
	DbPassword string
	DbHost     string
	DbSchema   string
	DbName     string
	DbPort     int

	ApiKeys []*ApiKeys
}

type MapperSourceData struct {
	Id             int64
	BridgeId       string
	Frame          string
	AppID          string  `json:"app_id"`
	DevID          string  `json:"dev_id"`
	HardwareSerial string  `json:"hardware_serial"`
	Port           int     `json:"port"`
	Counter        int     `json:"counter"`
	PayloadRaw     string  `json:"payload_raw"`
	Altitude       int     `json:"altitude"`
	Hdop           float64 `json:"hdop"`
	Latitude       float64 `json:"latitude"`
	Longitude      float64 `json:"longitude"`
	Geohash        string

	Metadata struct {
		Time       time.Time `json:"time"` // use string, because TTN sometimes sending empty strings instead of valid time value
		Frequency  float64   `json:"frequency"`
		Modulation string    `json:"modulation"`
		DataRate   string    `json:"data_rate"`
		Airtime    float64   `json:"airtime"`
		CodingRate string    `json:"coding_rate"`
		Gateways   []Gateway `json:"gateways"`
	} `json:"metadata"`
}

type Gateway struct {
	GtwID      string    `json:"gtw_id"`
	Timestamp  int64     `json:"timestamp,omitempty"`
	Time       time.Time `json:"time,omitempty"` // use string, because TTN sometimes sending empty strings instead of valid time value
	Channel    int       `json:"channel,omitempty"`
	Rssi       int       `json:"rssi,omitempty"`
	Snr        float64   `json:"snr,omitempty"`
	RfChain    int       `json:"rf_chain,omitempty"`
	Latitude   float64   `json:"latitude,omitempty"`
	Longitude  float64   `json:"longitude,omitempty"`
	Altitude   int       `json:"altitude,omitempty"`
	GtwTrusted bool      `json:"gtw_trusted,omitempty"`
	Geohash    string
}

type Route struct {
	Name     string
	Method   string
	Pattern  string
	IsPublic bool
	Handler  AppHandlerFunc
	AppCtx   *ApplicationContext
}

type AppHandlerFunc func(*Route) http.HandlerFunc
