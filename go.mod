module gitlab.com/csrnch/lora-mapper-dataservice

go 1.13

require (
	github.com/google/uuid v1.1.1
	github.com/gorilla/mux v1.7.4
	github.com/json-iterator/go v1.1.9
	github.com/lib/pq v1.5.2
	github.com/paulmach/go.geojson v1.4.0
	github.com/spf13/viper v1.7.0
)
