package handlers

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/csrnch/lora-mapper-dataservice/globals"
	"net/http"
	"strings"
	"time"

	"github.com/paulmach/go.geojson"
)

type GeoJsonHandlerVariables struct {
	BridgeId   string
	DeviceId   string
	RangeStart time.Time
	RangeEnd   time.Time
}

func GeojsonHandler(route *globals.Route) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		/* http://localhost:8080/geojson/prodesk/ttn-flight-position-tracker/202005170500/202005190500 */
		var handlerVariables GeoJsonHandlerVariables
		parserErr := parseGeoJsonHandlerVars(mux.Vars(r), &handlerVariables)

		if parserErr != nil {
			http.Error(w, parserErr.Error(), 500)
			return
		}

		res := ReadAll(route.AppCtx, handlerVariables.BridgeId, handlerVariables.DeviceId, handlerVariables.RangeStart, handlerVariables.RangeEnd)

		fc := geojson.NewFeatureCollection()

		var trackLine [][]float64

		for _, row := range res {
			for _, gw := range row.Metadata.Gateways {
				var gwLineStringFeature = geojson.NewLineStringFeature([][]float64{{gw.Longitude, gw.Latitude, float64(gw.Altitude)}, {row.Longitude, row.Latitude, float64(gw.Altitude)}})
				gwLineStringFeature.ID = "GW-REPORTS"
				props := map[string]interface{}{
					"A": fmt.Sprintf("GW:%s (%f,%f)", gw.GtwID, gw.Latitude, gw.Longitude),
					"B": gw.Rssi,
					"C": gw.Snr,
					"D": gw.Altitude,
				}

				gwLineStringFeature.Properties = props
				fc.AddFeature(gwLineStringFeature)

			}

			trackLine = append(trackLine, []float64{row.Longitude, row.Latitude, float64(row.Altitude)})

			var pointFeature = geojson.NewPointFeature([]float64{row.Longitude, row.Latitude, float64(row.Altitude)})

			pointProps := map[string]interface{}{
				"A": row.Id,
				"B": row.DevID,
				"C": row.Metadata.Time,
				"D": row.Metadata.Frequency,
				"E": row.Metadata.DataRate,
				"F": row.Metadata.Airtime,
				"G": row.Altitude,
			}

			pointFeature.Properties = pointProps
			pointFeature.ID = "TRACKING-POINTS"
			fc.AddFeature(pointFeature)
		}

		if len(res) > 0 {
			track := geojson.NewLineStringFeature(trackLine)
			track.ID = "TRACKING-LINE"
			fc.AddFeature(track)
		}

		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusOK)

		if err := json.NewEncoder(w).Encode(fc); err != nil {
			panic(err)
		}

	}
}

func parseGeoJsonHandlerVars(m map[string]string, g *GeoJsonHandlerVariables) error {
	g.BridgeId = m["bridgeId"]
	g.DeviceId = m["deviceId"]
	vRangeStart, err := time.Parse("200601021504", m["UTCrangeStart"])

	if len(strings.TrimSpace(g.BridgeId)) == 0 {
		return errors.New("Invalid BridgeID ")
	}

	if len(strings.TrimSpace(g.DeviceId)) == 0 {
		return errors.New("Invalid DeviceId")
	}

	if err != nil {
		return err
	}

	g.RangeStart = vRangeStart
	vRangeEnd, err := time.Parse("200601021504", m["UTCrangeEnd"])

	if err != nil {
		return err
	}

	g.RangeEnd = vRangeEnd
	return nil
}
