package handlers

import (
	"fmt"
	"gitlab.com/csrnch/lora-mapper-dataservice/globals"
	"log"
	"time"
)

type DataRow struct {
	FV_ID              int64
	FV_BRIDGE_ID       string
	FV_APP_ID          string
	FV_DEV_ID          string
	FV_HARDWARE_SERIAL string
	FV_ALTITUDE        int
	FV_LATITUDE        float64
	FV_LONGITUDE       float64
	FV_HDOP            float64
	FV_GEOHASH         string
	FV_PORT            int
	FV_COUNTER         int
	FV_PAYLOAD_RAW     string
	FV_TTN_TIME        time.Time
	FV_FREQUENCY       float64
	FV_MODULATION      string
	FV_DATA_RATE       string
	FV_AIRTIME         float64
	FV_CODING_RATE     string
	FV_FRAME           string
	FR_ID              int
	FR_BRIDGE_ID       string
	FR_FRAME           string
	FR_GTW_ID          string
	FR_TIMESTAMP       int64
	FR_TIME            time.Time
	FR_CHANNEL         int
	FR_RSSI            int
	FR_SNR             float64
	FR_RF_CHAIN        int
	FR_LATITUDE        float64
	FR_LONGITUDE       float64
	FR_GEOHASH         string
	FR_ALTITUDE        int
}

func ReadAll(appCtx *globals.ApplicationContext, bridgeId string, deviceId string, rangeStart time.Time, rangeEnd time.Time) []globals.MapperSourceData {

	var selectStatement = `select 
fv.id                as  fv_id,
fv.bridge_id         as  fv_bridge_id,
fv.app_id            as  fv_app_id,
fv.dev_id            as  fv_dev_id,
fv.hardware_serial   as  fv_hardware_serial,
fv.altitude          as  fv_altitude,
fv.latitude          as  fv_latitude,
fv.longitude         as  fv_longitude,
fv.hdop              as  fv_hdop,
fv.geohash           as  fv_geohash,
fv.port              as  fv_port,
fv.counter           as  fv_counter,
fv.payload_raw       as  fv_payload_raw,
fv.ttn_time          as  fv_ttn_time,
fv.frequency         as  fv_frequency,
fv.modulation        as  fv_modulation,
fv.data_rate         as  fv_data_rate,
fv.airtime           as  fv_airtime,
fv.coding_rate       as  fv_coding_rate,
fv.frame             as  fv_frame,
fr.id                as  fr_id,
fr.bridge_id         as fr_bridge_id,
fr.frame         as fr_frame,
fr.gtw_id        as fr_gtw_id,    
fr.timestamp     as fr_timestamp, 
fr.time          as fr_time,
fr.channel       as fr_channel,   
fr.rssi          as fr_rssi,  
fr.snr           as fr_snr,     
fr.rf_chain     as fr_rf_chain,
tt.latitude      as fr_latitude,
tt.longitude     as fr_longitude,
fr.geohash       as fr_geohash,
tt.altitude      as fr_altitude 
from 
	ttn_mapper_values as fv
inner join 
	ttn_mapper_reports as fr on fv.frame = fr.frame
inner join
	 ttn_gtw_basedata as tt on fr.gtw_id = tt.id
where
	fv.frame = fr.frame
	and fv.bridge_id = $1
	and fv.dev_id = $2
	and (fv.latitude > 0 and fv.longitude > 0)
	and (tt.latitude > 0 and tt.longitude > 0 and tt.latitude is not null and tt.longitude is not null)
	and fv.ttn_time between $3 and $4	
order by
	fv.ttn_time DESC, fv.frame, fr.frame LIMIT 1000000`

	log.Printf("Reading all values for debug....")

	rows, err := appCtx.DbConnection.Query(selectStatement, bridgeId, deviceId, rangeStart, rangeEnd)
	if err != nil {
		// handle this error better than this
		panic(err)
	}
	defer rows.Close()
	firstRow := true
	var result []globals.MapperSourceData

	var structured globals.MapperSourceData
	var lastFrame = ""

	for rows.Next() {

		var dataRow = DataRow{}

		err = rows.Scan(&dataRow.FV_ID,
			&dataRow.FV_BRIDGE_ID,
			&dataRow.FV_APP_ID,
			&dataRow.FV_DEV_ID,
			&dataRow.FV_HARDWARE_SERIAL,
			&dataRow.FV_ALTITUDE,
			&dataRow.FV_LATITUDE,
			&dataRow.FV_LONGITUDE,
			&dataRow.FV_HDOP,
			&dataRow.FV_GEOHASH,
			&dataRow.FV_PORT,
			&dataRow.FV_COUNTER,
			&dataRow.FV_PAYLOAD_RAW,
			&dataRow.FV_TTN_TIME,
			&dataRow.FV_FREQUENCY,
			&dataRow.FV_MODULATION,
			&dataRow.FV_DATA_RATE,
			&dataRow.FV_AIRTIME,
			&dataRow.FV_CODING_RATE,
			&dataRow.FV_FRAME,
			&dataRow.FR_ID,
			&dataRow.FR_BRIDGE_ID,
			&dataRow.FR_FRAME,
			&dataRow.FR_GTW_ID,
			&dataRow.FR_TIMESTAMP,
			&dataRow.FR_TIME,
			&dataRow.FR_CHANNEL,
			&dataRow.FR_RSSI,
			&dataRow.FR_SNR,
			&dataRow.FR_RF_CHAIN,
			&dataRow.FR_LATITUDE,
			&dataRow.FR_LONGITUDE,
			&dataRow.FR_GEOHASH,
			&dataRow.FR_ALTITUDE)

		if err != nil {
			fmt.Printf("error scanning row: ", err)
			return nil
		}

		if lastFrame != dataRow.FV_FRAME {
			if !firstRow {
				result = append(result, structured)
			}

			structured = globals.MapperSourceData{}
			structured.Id = dataRow.FV_ID
			structured.BridgeId = dataRow.FV_BRIDGE_ID
			structured.AppID = dataRow.FV_APP_ID
			structured.HardwareSerial = dataRow.FV_HARDWARE_SERIAL
			structured.Frame = dataRow.FV_FRAME
			structured.DevID = dataRow.FV_DEV_ID
			structured.PayloadRaw = dataRow.FV_PAYLOAD_RAW
			structured.Counter = dataRow.FV_COUNTER
			structured.Port = dataRow.FV_PORT
			structured.Altitude = dataRow.FV_ALTITUDE
			structured.Latitude = dataRow.FV_LATITUDE
			structured.Longitude = dataRow.FV_LONGITUDE
			structured.Geohash = dataRow.FV_GEOHASH
			structured.Hdop = dataRow.FV_HDOP
			structured.Metadata.Time = dataRow.FV_TTN_TIME
			structured.Metadata.CodingRate = dataRow.FV_CODING_RATE
			structured.Metadata.Airtime = dataRow.FV_AIRTIME
			structured.Metadata.Modulation = dataRow.FV_MODULATION
			structured.Metadata.Frequency = dataRow.FV_FREQUENCY
			structured.Metadata.DataRate = dataRow.FV_DATA_RATE
			gw := globals.Gateway{}
			gw.Time = dataRow.FR_TIME
			gw.Latitude = dataRow.FR_LATITUDE
			gw.Longitude = dataRow.FR_LONGITUDE
			gw.Altitude = dataRow.FR_ALTITUDE
			gw.GtwID = dataRow.FR_GTW_ID
			gw.RfChain = dataRow.FR_RF_CHAIN
			gw.Snr = dataRow.FR_SNR
			gw.Rssi = dataRow.FR_RSSI
			gw.Channel = dataRow.FR_CHANNEL
			gw.Timestamp = dataRow.FR_TIMESTAMP
			gw.Geohash = dataRow.FR_GEOHASH
			structured.Metadata.Gateways = append(structured.Metadata.Gateways, gw)

		} else {
			gw := globals.Gateway{}
			gw.Time = dataRow.FR_TIME
			gw.Latitude = dataRow.FR_LATITUDE
			gw.Longitude = dataRow.FR_LONGITUDE
			gw.Altitude = dataRow.FR_ALTITUDE
			gw.GtwID = dataRow.FR_GTW_ID
			gw.RfChain = dataRow.FR_RF_CHAIN
			gw.Snr = dataRow.FR_SNR
			gw.Rssi = dataRow.FR_RSSI
			gw.Channel = dataRow.FR_CHANNEL
			gw.Timestamp = dataRow.FR_TIMESTAMP
			gw.Geohash = dataRow.FR_GEOHASH
			structured.Metadata.Gateways = append(structured.Metadata.Gateways, gw)
		}

		lastFrame = dataRow.FV_FRAME
		firstRow = false

	}

	if !firstRow {
		result = append(result, structured)
		err = rows.Err()
	}

	if err != nil {
		fmt.Printf("error scanning row: ", err)
		return nil
	}
	return result
}
