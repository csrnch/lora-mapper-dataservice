package handlers

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"gitlab.com/csrnch/lora-mapper-dataservice/globals"
	"log"
)

func OpenDatabaseConnection(appCtx *globals.ApplicationContext) {

	db, err := sql.Open("postgres", fmt.Sprintf("host=%s port=%d dbname=%s user=%s password=%s search_path=%s", appCtx.DbHost, appCtx.DbPort, appCtx.DbName, appCtx.DbUser, appCtx.DbPassword, appCtx.DbSchema))
	log.Printf("Opening Database connection")

	if err != nil {
		log.Printf("Could not open database connection  %s")
		log.Fatal(err)
	}

	err = db.Ping()
	if err != nil {
		log.Printf("Could not connect database")
		log.Fatal(err)
	}

	appCtx.DbConnection = db

}

func CloseDbConn(appCtx *globals.ApplicationContext) {

	log.Printf("Closing DB connection")
	err := appCtx.DbConnection.Close()

	if err != nil {
		log.Panicf("could not close database conneciton: %s", err)
	}

	log.Print("...closed")

}
