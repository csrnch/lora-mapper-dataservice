package handlers

import (
	"fmt"
	"github.com/google/uuid"
	"gitlab.com/csrnch/lora-mapper-dataservice/globals"
	"log"
	"net/http"
)

func HttpAuthChainHandler(inner http.Handler, route *globals.Route) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		requestSerial := fmt.Sprintf("mr-public-%s", uuid.New().String())

		if route.IsPublic {
			w.Header().Set("X-mapper-requestserial", requestSerial)
			r.Header.Set("X-mapper-requestserial", requestSerial)

			log.Printf(
				"AUTH:PUBLIC Method: %s RequestURI:%s Route:%s IP:%s RequestSerial:%s",
				r.Method,
				r.RequestURI,
				route.Name,
				GetIP(r),
				requestSerial,
			)

			inner.ServeHTTP(w, r)
			return
		}

		var apiKey = getAuthMapperKey(r)
		var apiUser = GetAuthMapperUser(r)

		var userAuthed = false

		for _, rKey := range route.AppCtx.ApiKeys {
			if rKey.User == apiUser && rKey.Key == apiKey {
				userAuthed = true
				requestSerial = fmt.Sprintf("mr-%s-%s", rKey.User, uuid.New().String())
				w.Header().Set("X-mapper-requestserial", requestSerial)
				r.Header.Set("X-mapper-requestserial", requestSerial)
				break
			}
		}

		if userAuthed {
			log.Printf(
				"AUTH:AUTHORIZED User:%s Method: %s RequestURI:%s Route:%s IP:%s RequestSerial:%s",
				apiUser,
				r.Method,
				r.RequestURI,
				route.Name,
				GetIP(r),
				requestSerial,
			)

			inner.ServeHTTP(w, r)
		} else {
			log.Printf(
				"NOT AUTHORIZED (401): Method: %s RequestURI:%s Route:%s IP:%s",
				r.Method,
				r.RequestURI,
				route.Name,
				GetIP(r),
			)

			w.WriteHeader(http.StatusUnauthorized)
		}

	}
}

func GetAuthMapperUser(r *http.Request) string {
	return r.Header.Get("X-mapper-user")
}

func GetAuthMapperRequestSerial(r *http.Request) string {
	return r.Header.Get("X-mapper-requestserial")
}

func getAuthMapperKey(r *http.Request) string {
	return r.Header.Get("X-mapper-key")
}

func GetIP(r *http.Request) string {
	forwarded := r.Header.Get("X-FORWARDED-FOR")
	if forwarded != "" {
		return forwarded
	}
	return r.RemoteAddr
}
