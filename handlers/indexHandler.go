package handlers

import (
	"encoding/json"
	"gitlab.com/csrnch/lora-mapper-dataservice/globals"
	"net/http"
)

func IndexHandler(route *globals.Route) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {
		appStatus := globals.AppStatus{Status: "Active", Connected: true}

		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusOK)

		if err := json.NewEncoder(w).Encode(appStatus); err != nil {
			panic(err)
		}
	}
}
