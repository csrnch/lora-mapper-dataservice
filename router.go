package main

import (
	"gitlab.com/csrnch/lora-mapper-dataservice/globals"
	"gitlab.com/csrnch/lora-mapper-dataservice/handlers"
	"net/http"

	"github.com/gorilla/mux"
)

func CreateRouter() *mux.Router {

	var routes = []globals.Route{
		globals.Route{
			"Index",
			"GET",
			"/",
			true,
			handlers.IndexHandler,
			appCtx,
		},
		globals.Route{
			"Geojson",
			"GET",
			"/geojson/{bridgeId}/{deviceId}/{UTCrangeStart}/{UTCrangeEnd}", /* http://localhost:8080/geojson/prodesk/ttn-flight-position-tracker/202005181540/202005181600 */
			false,
			handlers.GeojsonHandler,
			appCtx,
		},
	}

	router := mux.NewRouter().StrictSlash(true)
	for idx, _ := range routes {
		route := routes[idx]

		var handler http.Handler
		handler = route.Handler(&route)
		handler = addCommonHandlers(handler, &route)

		router.
			Methods(route.Method).
			Path(route.Pattern).
			Name(route.Name).
			Handler(handler)

		router.Use()
	}
	return router
}

func addCommonHandlers(handler http.Handler, route *globals.Route) http.Handler {

	handler = handlers.LoggerChainHandler(handler, route)
	handler = handlers.HttpAuthChainHandler(handler, route)

	return handler
}
