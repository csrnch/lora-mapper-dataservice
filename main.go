package main

import (
	"github.com/spf13/viper"
	"gitlab.com/csrnch/lora-mapper-dataservice/globals"
	"gitlab.com/csrnch/lora-mapper-dataservice/handlers"
	"os"
	"os/signal"
	"syscall"

	"log"
	"net/http"
)

var appCtx *globals.ApplicationContext

func main() {
	var appContext globals.ApplicationContext
	appCtx = &appContext
	readApplicationConfig()
	handlers.OpenDatabaseConnection(appCtx)
	defer handlers.CloseDbConn(appCtx)

	go func() {
		router := CreateRouter()
		log.Fatal(http.ListenAndServe(":8080", router))
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)

	<-c
}

func readApplicationConfig() {
	viper.SetConfigName("lmd")
	viper.SetConfigType("json")
	viper.AddConfigPath("/etc/lmd/")
	viper.AddConfigPath("$HOME/.lmd")
	viper.AddConfigPath("./configs")

	err := viper.ReadInConfig()
	if err != nil { // Handle errors reading the config file
		log.Panicf("fatal error: %s", err)
	}
	viper.Unmarshal(&appCtx)
}
